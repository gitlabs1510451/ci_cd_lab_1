import sys

def calculate_power(a, n):
    return pow(a, n)

if __name__ == "__main__":
    a = float(sys.argv[1])
    n = int(sys.argv[2])
    result = calculate_power(a, n)
    print(result)
